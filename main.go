package main

import (
	"fmt"
	"net/http"
)

func handler(w http.ResponseWriter, r *http.Request) {
	html := `
	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100..900;1,100..900&family=Open+Sans:ital,wght@0,300..800;1,300..800&display=swap" rel="stylesheet">
		<title>Image Centering</title>
		<style>
			body, html {
			  	font-family: "Montserrat", sans-serif;
  				font-optical-sizing: auto;
				height: 100%;
				margin: 0;
				display: flex;
				align-items: center;
				justify-content: center;
				background-color: #f0f0f0;
			}
			.container {
				text-align: center;
			}
			img {
				height: 50%;
				width: 50%;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<img src="/static/gopher.png" width="300" height="300" alt="Centered Image">
			<p>Service Unavailable</p>
			<br>
			<p>Refactoring BPF simulator</p>
		</div>
	</body>
	</html>
	`
	fmt.Fprintf(w, html)
}

func main() {
	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.HandleFunc("/", handler)

	fmt.Println("Server starting at http://localhost:8080")
	if err := http.ListenAndServe(":8081", nil); err != nil {
		fmt.Println("Server failed to start:", err)
	}
}
